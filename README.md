# SeleniumTests

Coding Dojo permettant de prendre en main L'outil selenium pour l'automatisation de tests.

Les cas d'utilisation : recherche google, l'application DOJO AMS

Les points à acquérir
- rappel sur les xpath
- la lib Selenium et Webdriver
- quelle architecture pour des tests maintenables : pages vs scénarios
- quelles limites et exigences avec une application Angular, technos alternatives



