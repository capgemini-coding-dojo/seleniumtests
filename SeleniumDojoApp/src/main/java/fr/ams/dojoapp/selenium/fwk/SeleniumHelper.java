package fr.ams.dojoapp.selenium.fwk;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Méthodes utilitaires pour les tests Selenium.
 *
 * @author gthoonse
 *
 */
public final class SeleniumHelper {

    private static final Log LOG = LogFactory.getLog(SeleniumHelper.class);

    /**
     * Classe de type helper à utiliser de façon statique : constructeur privé.
     */
    private SeleniumHelper() {
        super();
    }

    /**
     * Attend le temps demandé en ms. Autant que possible cette méthode ne doit pas
     * être utilisée : privilégier des attentes actives basées sur la présence d'un
     * composant du DOM. Rechercher une meilleure solution si on souhaite accélérer
     * le déroulement des tests.
     */
    public static void wait(int timeMillis) {
        try {
            LOG.info("Attente demandée en ms : " + timeMillis);
            Thread.sleep(timeMillis);
            LOG.info("Fin d'attente demandée");
        } catch (InterruptedException ie) {
            LOG.error("Interrupted Exception" + ie);
        }
    }

    /**
     * Attend le temps demandé en secondes. Autant que possible cette méthode ne
     * doit pas être utilisée : privilégier des attentes actives basées sur la
     * présence d'un composant du DOM. Rechercher une meilleure solution si on
     * souhaite accélérer le déroulement des tests.
     */
    public static void waitSecs(int timeSecs) {

        LOG.info("Attente demandée en s : " + timeSecs);
        wait(timeSecs * 1000);
    }

    /**
     * Attend le temps nécessaire à l'attente d'une action longue à s'exécuter. Le
     * temps est paramétrable au lancement du smoketest (cf classe
     * TestsConfiguration)
     * 
     * Autant que possible cette méthode ne doit pas être utilisée : privilégier des
     * attentes actives basées sur la présence d'un composant du DOM.
     */
    public static void waitLongDelay() {
        SeleniumHelper.wait(TestsConfiguration.getLongWaitDelay() * 1000);
    }

    /**
     * Attend le temps standard nécessaire à l'attente de l'exécution d'une action.
     * Le temps est paramétrable au lancement du smoketest (cf classe
     * TestsConfiguration)
     * 
     * Autant que possible cette méthode ne doit pas être utilisée : privilégier des
     * attentes actives basées sur la présence d'un composant du DOM.
     */
    public static void waitStandardDelay() {
        SeleniumHelper.wait(TestsConfiguration.getStandardWaitDelay() * 1000);
    }

}
