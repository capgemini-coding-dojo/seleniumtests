package fr.ams.dojoapp.selenium.fwk;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Gère la configuration des tests.
 *
 */

public final class TestsConfiguration {

    private static final Log LOG = LogFactory.getLog(TestsConfiguration.class);

    // délai d'attente long par défaut : 60 secondes
    private static final int DEFAULT_LONG_WAIT_DELAY = 60;

    // délai d'attente standard par défaut : 5 secondes
    private static final int DEFAULT_STD_WAIT_DELAY = 10;

    // délai d'attente standard - cache
    private static int stdWaitDelay = -1;

    // délai d'attente long - cache
    private static int longWaitDelay = -1;

    /**
     * Classe de type helper à utiliser de façon statique : constructeur privé.
     */
    private TestsConfiguration() {
        super();
    }

    /**
     * Retourne l'url qui permet de tester l'application.
     *
     * @return
     */
    public static String getAppUrl() {

        String appUrl = "http://localhost:8090";

        return appUrl;
    }

    /**
     * Indique si le web driver Selenium doit être activé en local. Sinon il doit
     * etre activé en distant
     *
     * @return le driver instancié
     */
    public static boolean isWebDriverLocal() {

        return true;
    }

    /**
     * Retourne le chemin d'accès au ChromeDriver Selenium utilisé en mode local.
     *
     * @return
     */
    public static String getWebDriverPath() {

        return "c:\\progs\\chromedriver.exe";

    }

    /**
     * Retourne le délai en s à attendre avant de fermer le navigateur en fin de
     * test.
     *
     * @return
     */
    public static int getSleepDelayAfterTest() {

        int intSleepDelay = 5000;

        return intSleepDelay;
    }

    /**
     * Retourne le délai en s à utiliser dans les cas d'attente longue à effectuer
     * dans les tests.
     *
     * @return
     */
    public static int getLongWaitDelay() {

        return 10;
    }

    /**
     * Retourne le délai en s à utiliser dans les cas d'attente standard à effectuer
     * dans les tests.
     *
     * @return
     */
    public static int getStandardWaitDelay() {

        return 1;
    }

}
