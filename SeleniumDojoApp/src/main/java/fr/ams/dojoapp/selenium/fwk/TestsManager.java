package fr.ams.dojoapp.selenium.fwk;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Gère techniquement le WebDriver utilisé pour les tests Cette classe permet
 * d'isoler le type de driver choisi, son paramétrage, son instanciation et sa
 * fermeture.
 * 
 * 
 * @author gthoonse
 *
 */

public final class TestsManager {

    private static final Log LOG = LogFactory.getLog(TestsManager.class);

    /**
     * Classe de type helper à utiliser de façon statique : constructeur privé.
     */
    private TestsManager() {
        super();
    }

    /**
     * Instancie un web driver Selenium local en fonction du paramétrage fixé.
     * 
     * @return le driver instancié
     */
    public static WebDriver createLocalSeleniumDriver() {
        WebDriver driver;

        // utilisation Locale
        // télécharger le chrome driver ici :
        // http://chromedriver.storage.googleapis.com/index.htm

        System.setProperty("webdriver.chrome.driver", TestsConfiguration.getWebDriverPath());
        LOG.debug("Instanciation du driver Selenium Chrome Local");
        driver = new ChromeDriver();
        return driver;
    }

    /**
     * Ferme le Navigateur Web. Si un délai d'attente est spécifié par propriété
     * Système, il est respecté.
     * 
     * @throws Exception
     */
    public static void close(WebDriver driver) throws Exception {

        LOG.debug("entrée closeSeleniumDriver");

        int sleepDelay = TestsConfiguration.getSleepDelayAfterTest();

        if (sleepDelay != 0) {
            LOG.debug("Début d'attente avant la fermeture du browser : " + sleepDelay);
            Thread.sleep(sleepDelay);
            LOG.debug("Fin d'attente avant la fermeture du browser : " + sleepDelay);
        }
        driver.quit();
        LOG.debug("fin closeSeleniumDriver - Browser fermé");

    }

}
