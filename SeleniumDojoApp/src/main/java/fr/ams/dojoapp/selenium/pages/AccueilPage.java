﻿package fr.ams.dojoapp.selenium.pages;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import fr.ams.dojoapp.selenium.fwk.TestsConfiguration;

/**
 * Classe masquant les éléments techniques de l'écran d'accueil de l'appli DOJO
 * 
 * @author gthoonse
 */

public class AccueilPage extends SeleniumPage {

    private static final Log LOG = LogFactory.getLog(AccueilPage.class);

    public static String TITRE_FENETRE_ACCUEIL = "CapgeminiDojoMedia";

    public static String TITRE_ECRAN_ACCUEIL = "Capgemini DOJO Media";

    /**
     * Retourne le titre de la page d'accueil
     * 
     * @return
     */
    public String getTitreEcran() {

        String titreEcran = driver.findElement(By.xpath("//app-header/div/div/h1")).getText();

        LOG.debug("Titre de l'écran : " + titreEcran);

        return titreEcran;

    }

    /**
     * Constructeur de la page d'authent. Dirige le navigateur vers l'URL et attend
     * que la page soit construite (contrôle de la présence du bouton "valider")
     * 
     * @param driver
     * @param wait
     */
    public AccueilPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);

        // Positionne le navigateur sur la page correspondant à l'url définie
        String appUrl = TestsConfiguration.getAppUrl();
        LOG.debug("Constructeur - accès à la page d'accueil d'url : " + appUrl);
        driver.get(appUrl);

        // On attend que le titre de la page soit l'attendu
        wait.until(ExpectedConditions.titleIs(TITRE_FENETRE_ACCUEIL));

        LOG.debug("Constructeur - Page d'authentification instanciée et chargée");

    }


}
