package fr.ams.dojoapp.selenium.pages;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class SeleniumPage {

    private static final Log LOG = LogFactory.getLog(SeleniumPage.class);

    protected WebDriver driver;

    protected WebDriverWait wait;

    public SeleniumPage(WebDriver driver, WebDriverWait wait) {
        super();
        this.driver = driver;
        this.wait = wait;

        LOG.debug("Instantiation OK d'une page " + this.getClass().getName());

        // Bonne pratique :
        // check that the WebDriver is on the correct page when we instantiate the
        // PageObject
        // ==> à faire dans les constructeurs des sous classes
    }

}
