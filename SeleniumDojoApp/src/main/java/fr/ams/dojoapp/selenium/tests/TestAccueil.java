﻿package fr.ams.dojoapp.selenium.tests;

import static org.junit.Assert.assertEquals;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import fr.ams.dojoapp.selenium.fwk.TestsConfiguration;
import fr.ams.dojoapp.selenium.fwk.TestsManager;
import fr.ams.dojoapp.selenium.pages.AccueilPage;

public class TestAccueil {

    private static final Log LOG = LogFactory.getLog(TestAccueil.class);

    private WebDriver driver;

    private WebDriverWait wait;

    @Before
    public void setUp() {

        driver = TestsManager.createLocalSeleniumDriver();

        // Wait avec un timeout de 1 seconde
        wait = new WebDriverWait(driver, TestsConfiguration.getStandardWaitDelay());

    }

    /**
     * Scénario de lancement simple de l'application
     */
    @Test
    public void testLancement() {

        LOG.debug("testConnexion : Lancement");

        AccueilPage pageAccueil = new AccueilPage(driver, wait);

        // Controler qu'on est sur la bonne application : page d'accueil DOJO par un
        // libellé dans l'IHM
        // note : le titre de la page a été contrôlé lors de l'authentification
        assertEquals("La page affichée doit être l'accueil DOJO, avec le bon titre dans l'écran", AccueilPage.TITRE_ECRAN_ACCUEIL, pageAccueil.getTitreEcran());

        LOG.debug("testConnexion : Page d'accueil OK");

    }

    @After
    public void tearDown() throws Exception {

        TestsManager.close(driver);
    }

}
