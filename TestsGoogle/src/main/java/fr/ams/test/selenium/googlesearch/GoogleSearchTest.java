﻿package fr.ams.test.selenium.googlesearch;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Classe de test simple : effectuer des recherches Google
 * 
 * @author gthoonse
 */

public class GoogleSearchTest {

    private static final Log LOG = LogFactory.getLog(GoogleSearchTest.class);

    // Web driver Selenium
    private WebDriver driver;

    // temps d'attente standard entre actions, en secondes
    private static int STD_WAIT_TIMEOUT = 5;

    @Before
    public void setUp() {

        System.setProperty("webdriver.chrome.driver", "c:\\progs\\chromedriver.exe");
        LOG.debug("Instanciation du driver Selenium Chrome Local");
        driver = new ChromeDriver();

    }

    @Test
    public void testPagesMarvel() {

        LOG.debug("=== DEBUT DU TEST MARVEL ===");

        driver.get("https://www.google.com/");
        driver.findElement(By.name("q")).sendKeys("marvel", Keys.RETURN);

        // Attente d'un retour
        new WebDriverWait(driver, STD_WAIT_TIMEOUT).until(ExpectedConditions.titleIs("marvel - Recherche Google"));
        LOG.debug("apres wait");

        // Recherche des stats sur les résultats
        WebElement stats = driver.findElement(By.xpath("//*[@id=\"resultStats\"]"));
        LOG.debug("Nombre de resultats : " + stats.getText());

        LOG.debug("=== FIN DU TEST MARVEL ===");
    }

 

    @After
    public void tearDown() throws Exception {
        driver.quit();
    }

}
